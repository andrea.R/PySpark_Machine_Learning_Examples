# Machine Learning Projects with PySpark

A bunch of different machine learning projects implemented in PySpark. We have projects for the main types of machine learning algorithms (such as logistic regression, tree-based classification and K-means clustering).
